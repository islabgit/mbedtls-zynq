/*  
 * crypto_aria_alt.c
 *
 *  Created on: 2018. 8. 23.
 *      Author: asepawal
 */

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_ARIA_C)

#include "mbedtls/aria.h"


#include <string.h>

#if defined(MBEDTLS_SELF_TEST)
#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#define mbedtls_printf printf
#endif /* MBEDTLS_PLATFORM_C */
#endif /* MBEDTLS_SELF_TEST */

#if defined(MBEDTLS_ARIA_ALT)

#include "mbedtls/platform_util.h"
#include "cryptochip/aria_alt.h"

void core_aria_init( mbedtls_aria_context *ctx ){
	int fd = open ("/dev/mem", O_RDWR);
	unsigned page_addr, page_offset;
	unsigned page_size = sysconf(_SC_PAGESIZE);
	
	page_addr = (CORE_BASE_ADDR & (~(page_size-1)));
	page_offset = CORE_BASE_ADDR - page_addr;
	
	if (fd < 1) {
		printf("ERROR: core_aria_init()!!\n");
		sleep(3);
		return;
	}
	ctx->hw_aria = mmap(NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr);
}

/**
 * \brief          This function initializes the specified ARIA context.
 *
 *                 It must be the first API called before using
 *                 the context.
 *
 * \param ctx      The ARIA context to initialize.
 */
void mbedtls_aria_init( mbedtls_aria_context *ctx )
{
	memset(ctx, 0, sizeof(mbedtls_aria_context));

	core_aria_init(ctx);

}

/**
 * \brief          This function releases and clears the specified ARIA context.
 *
 * \param ctx      The ARIA context to clear.
 */
void mbedtls_aria_free( mbedtls_aria_context *ctx )
{
    if( ctx == NULL )
        return;

    memset( ctx, 0, sizeof( mbedtls_aria_context ) );
}

/**
 * \brief          This function sets the encryption key.
 *
 * \param ctx      The ARIA context to which the key should be bound.
 * \param key      The encryption key.
 * \param keybits  The size of data passed in bits. Valid options are:
 *                 <ul><li>128 bits</li>
 *                 <li>192 bits</li>
 *                 <li>256 bits</li></ul>
 *
 * \return         \c 0 on success or #MBEDTLS_ERR_ARIA_INVALID_KEY_LENGTH
 *                 on failure.
 */
int mbedtls_aria_setkey_enc( mbedtls_aria_context *ctx,
                             const unsigned char *key,
                             unsigned int keybits )
{
	int keylen = keybits / 8;

	memset(ctx->key, 0, 32); //clear all key bytes
	memcpy(ctx->key, key, keylen);

	if (keylen == 16) {
		ctx->mode |= ARIA_KEY_128 << ARIA_MODE_BIT_KEY_SIZE;
	} else if (keylen == 24) {
		ctx->mode |= ARIA_KEY_192 << ARIA_MODE_BIT_KEY_SIZE;
	} else if (keylen == 32) {
		ctx->mode |= ARIA_KEY_256 << ARIA_MODE_BIT_KEY_SIZE;
	} else {
		printf("Error: key size is not supported! %d", keylen);
		sleep(3);
		return -1;
	}

	ctx->mode &= ~(1 << ARIA_MODE_BIT_FLAG_DEC);

	return 0;
}

/**
 * \brief          This function sets the decryption key.
 *
 * \param ctx      The ARIA context to which the key should be bound.
 * \param key      The decryption key.
 * \param keybits  The size of data passed. Valid options are:
 *                 <ul><li>128 bits</li>
 *                 <li>192 bits</li>
 *                 <li>256 bits</li></ul>
 *
 * \return         \c 0 on success, or #MBEDTLS_ERR_ARIA_INVALID_KEY_LENGTH on failure.
 */
int mbedtls_aria_setkey_dec( mbedtls_aria_context *ctx,
                             const unsigned char *key,
                             unsigned int keybits )
{
	int keylen = keybits / 8;

	memset(ctx->key, 0, 32); //clear all key bytes
	memcpy(ctx->key, key, keylen);

	if (keylen == 16) {
		ctx->mode |= ARIA_KEY_128 << ARIA_MODE_BIT_KEY_SIZE;
	} else if (keylen == 24) {
		ctx->mode |= ARIA_KEY_192 << ARIA_MODE_BIT_KEY_SIZE;
	} else if (keylen == 32) {
		ctx->mode |= ARIA_KEY_256 << ARIA_MODE_BIT_KEY_SIZE;
	} else {
		printf("Error: key size is not supported! %d", keylen);
		sleep(3);
		return -1;
	}

	ctx->mode |= 1 << ARIA_MODE_BIT_FLAG_DEC;

	return 0;
}

/**
 * \brief          This function performs an ARIA single-block encryption or
 *                 decryption operation.
 *
 *                 It performs encryption or decryption (depending on whether
 *                 the key was set for encryption on decryption) on the input
 *                 data buffer defined in the \p input parameter.
 *
 *                 mbedtls_aria_init(), and either mbedtls_aria_setkey_enc() or
 *                 mbedtls_aria_setkey_dec() must be called before the first
 *                 call to this API with the same context.
 *
 * \param ctx      The ARIA context to use for encryption or decryption.
 * \param input    The 16-Byte buffer holding the input data.
 * \param output   The 16-Byte buffer holding the output data.

 * \return         \c 0 on success.
 */
int mbedtls_aria_crypt_ecb( mbedtls_aria_context *ctx,
                            const unsigned char input[MBEDTLS_ARIA_BLOCKSIZE],
                            unsigned char output[MBEDTLS_ARIA_BLOCKSIZE] )
{

	ctx->mode |= 1 << ARIA_MODE_BIT_ECB;

	//operation
	ctx->hw_aria->operation = ARIA_CLEAR;
	ctx->hw_aria->mode = ctx->mode;
	ctx->hw_aria->add_len = 0;
	ctx->hw_aria->msg_len = MBEDTLS_ARIA_BLOCKSIZE;
	ctx->hw_aria->operation = ARIA_INIT;
	memcpy(ctx->hw_aria->key, ctx->key, 32);
	ctx->hw_aria->operation = ARIA_UPDATE_KEY;
	memcpy(ctx->hw_aria->blk_in, input, MBEDTLS_ARIA_BLOCKSIZE);
	ctx->hw_aria->operation = ARIA_UPDATE_BLK;
	memcpy(output, ctx->hw_aria->blk_out, MBEDTLS_ARIA_BLOCKSIZE);

	return 0;
}


#if defined(MBEDTLS_CIPHER_MODE_CBC)
/**
 * \brief  This function performs an ARIA-CBC encryption or decryption operation
 *         on full blocks.
 *
 *         It performs the operation defined in the \p mode
 *         parameter (encrypt/decrypt), on the input data buffer defined in
 *         the \p input parameter.
 *
 *         It can be called as many times as needed, until all the input
 *         data is processed. mbedtls_aria_init(), and either
 *         mbedtls_aria_setkey_enc() or mbedtls_aria_setkey_dec() must be called
 *         before the first call to this API with the same context.
 *
 * \note   This function operates on aligned blocks, that is, the input size
 *         must be a multiple of the ARIA block size of 16 Bytes.
 *
 * \note   Upon exit, the content of the IV is updated so that you can
 *         call the same function again on the next
 *         block(s) of data and get the same result as if it was
 *         encrypted in one call. This allows a "streaming" usage.
 *         If you need to retain the contents of the IV, you should
 *         either save it manually or use the cipher module instead.
 *
 *
 * \param ctx      The ARIA context to use for encryption or decryption.
 * \param mode     The ARIA operation: #MBEDTLS_ARIA_ENCRYPT or
 *                 #MBEDTLS_ARIA_DECRYPT.
 * \param length   The length of the input data in Bytes. This must be a
 *                 multiple of the block size (16 Bytes).
 * \param iv       Initialization vector (updated after use).
 * \param input    The buffer holding the input data.
 * \param output   The buffer holding the output data.
 *
 * \return         \c 0 on success, or #MBEDTLS_ERR_ARIA_INVALID_INPUT_LENGTH
 *                 on failure.
 */
int mbedtls_aria_crypt_cbc( mbedtls_aria_context *ctx,
                            int mode,
                            size_t length,
                            unsigned char iv[MBEDTLS_ARIA_BLOCKSIZE],
                            const unsigned char *input,
                            unsigned char *output )
{
	u32 i, block;
	ctx->mode |= 1 << ARIA_MODE_BIT_CBC;

	ctx->mode |= MBEDTLS_ARIA_BLOCKSIZE << ARIA_MODE_BIT_IV_SIZE;

	if (mode) {
		ctx->mode &= ~(1 << ARIA_MODE_BIT_FLAG_DEC);
	} else {
		ctx->mode |= 1 << ARIA_MODE_BIT_FLAG_DEC;
	}

	//operation
	ctx->hw_aria->operation = ARIA_CLEAR;
	ctx->hw_aria->mode = ctx->mode;
	ctx->hw_aria->add_len = 0;
	ctx->hw_aria->msg_len = length;
	ctx->hw_aria->operation = ARIA_INIT;
	memcpy(ctx->hw_aria->key, ctx->key, 32);
	ctx->hw_aria->operation = ARIA_UPDATE_KEY;
	memcpy(ctx->hw_aria->blk_in, iv, MBEDTLS_ARIA_BLOCKSIZE);
	ctx->hw_aria->operation = ARIA_UPDATE_IV;

	for(i = 0; i < length; i += MBEDTLS_ARIA_BLOCKSIZE){
		block = ((length-1)%MBEDTLS_ARIA_BLOCKSIZE) + 1;
		memcpy(ctx->hw_aria->blk_in, input + i, block);
		ctx->hw_aria->operation = ARIA_UPDATE_BLK;
		memcpy(output + i, ctx->hw_aria->blk_out, block);
	}

	return 0;
}
#endif /* MBEDTLS_CIPHER_MODE_CBC */

#if defined(MBEDTLS_CIPHER_MODE_CTR)
/**
 * \brief      This function performs an ARIA-CTR encryption or decryption
 *             operation.
 *
 *             This function performs the operation defined in the \p mode
 *             parameter (encrypt/decrypt), on the input data buffer
 *             defined in the \p input parameter.
 *
 *             Due to the nature of CTR, you must use the same key schedule
 *             for both encryption and decryption operations. Therefore, you
 *             must use the context initialized with mbedtls_aria_setkey_enc()
 *             for both #MBEDTLS_ARIA_ENCRYPT and #MBEDTLS_ARIA_DECRYPT.
 *
 * \warning    You must never reuse a nonce value with the same key. Doing so
 *             would void the encryption for the two messages encrypted with
 *             the same nonce and key.
 *
 *             There are two common strategies for managing nonces with CTR:
 *
 *             1. You can handle everything as a single message processed over
 *             successive calls to this function. In that case, you want to
 *             set \p nonce_counter and \p nc_off to 0 for the first call, and
 *             then preserve the values of \p nonce_counter, \p nc_off and \p
 *             stream_block across calls to this function as they will be
 *             updated by this function.
 *
 *             With this strategy, you must not encrypt more than 2**128
 *             blocks of data with the same key.
 *
 *             2. You can encrypt separate messages by dividing the \p
 *             nonce_counter buffer in two areas: the first one used for a
 *             per-message nonce, handled by yourself, and the second one
 *             updated by this function internally.
 *
 *             For example, you might reserve the first 12 bytes for the
 *             per-message nonce, and the last 4 bytes for internal use. In that
 *             case, before calling this function on a new message you need to
 *             set the first 12 bytes of \p nonce_counter to your chosen nonce
 *             value, the last 4 to 0, and \p nc_off to 0 (which will cause \p
 *             stream_block to be ignored). That way, you can encrypt at most
 *             2**96 messages of up to 2**32 blocks each with the same key.
 *
 *             The per-message nonce (or information sufficient to reconstruct
 *             it) needs to be communicated with the ciphertext and must be unique.
 *             The recommended way to ensure uniqueness is to use a message
 *             counter. An alternative is to generate random nonces, but this
 *             limits the number of messages that can be securely encrypted:
 *             for example, with 96-bit random nonces, you should not encrypt
 *             more than 2**32 messages with the same key.
 *
 *             Note that for both stategies, sizes are measured in blocks and
 *             that an ARIA block is 16 bytes.
 *
 * \warning    Upon return, \p stream_block contains sensitive data. Its
 *             content must not be written to insecure storage and should be
 *             securely discarded as soon as it's no longer needed.
 *
 * \param ctx              The ARIA context to use for encryption or decryption.
 * \param length           The length of the input data.
 * \param nc_off           The offset in the current \p stream_block, for
 *                         resuming within the current cipher stream. The
 *                         offset pointer should be 0 at the start of a stream.
 * \param nonce_counter    The 128-bit nonce and counter.
 * \param stream_block     The saved stream block for resuming. This is
 *                         overwritten by the function.
 * \param input            The buffer holding the input data.
 * \param output           The buffer holding the output data.
 *
 * \return     \c 0 on success.
 */
int mbedtls_aria_crypt_ctr( mbedtls_aria_context *ctx,
                            size_t length,
                            size_t *nc_off,
                            unsigned char nonce_counter[MBEDTLS_ARIA_BLOCKSIZE],
                            unsigned char stream_block[MBEDTLS_ARIA_BLOCKSIZE],
                            const unsigned char *input,
                            unsigned char *output )

{
//	crypto_aria_context *aria_ctx = (crypto_aria_context *) cryptochip_get_context(ARIA_CTX);
//
//	aria_ctx->moo = ARIA_MODE_BIT_CTR;
//	aria_ctx->isDecrypt = ctx->mode;
//	aria_ctx->key = ctx->key;
//	aria_ctx->keylen = ctx->keylen;
//	aria_ctx->iv = (u8*)nonce_counter;
//	aria_ctx->ivlen = MBEDTLS_ARIA_BLOCKSIZE;
//	aria_ctx->addlen = 0;
//	aria_ctx->input = (u8*)input;
//	aria_ctx->output = output;
//	aria_ctx->msglen = length;
//
//	return cryptochip_aria_crypt(aria_ctx);
	return 0;
}

#endif /* MBEDTLS_CIPHER_MODE_CTR */

#if defined(MBEDTLS_CIPHER_MODE_OFB)
/**
 * \brief This function performs an ARIA-OFB encryption or decryption
 *        operation.
 *
 *        It performs the operation defined in the \p mode
 *        parameter (encrypt or decrypt), on the input data buffer
 *        defined in the \p input parameter.
 *
 *        For OFB, you must set up the context with mbedtls_aria_setkey_enc(),
 *        regardless of whether you are performing an encryption or decryption
 *        operation, that is, regardless of the \p mode parameter. This is
 *        because OFB mode uses the same key schedule for encryption and
 *        decryption.
 *
 * \note  Upon exit, the content of the IV is updated so that you can
 *        call the same function again on the next
 *        block(s) of data and get the same result as if it was
 *        encrypted in one call. This allows a "streaming" usage.
 *        If you need to retain the contents of the
 *        IV, you must either save it manually or use the cipher
 *        module instead.
 *
 *
 * \param ctx      The ARIA context to use for encryption or decryption.
 * \param mode     The ARIA operation: #MBEDTLS_ARIA_ENCRYPT or
 *                 #MBEDTLS_ARIA_DECRYPT.
 * \param length   The length of the input data.
 * \param iv_off   The offset in IV (updated after use).
 * \param iv       The initialization vector (updated after use).
 * \param input    The buffer holding the input data.
 * \param output   The buffer holding the output data.
 *
 * \return         \c 0 on success.
 */
int mbedtls_aria_crypt_ofb( mbedtls_aria_context *ctx,
                              	// int mode,
                               size_t length,
                               size_t *iv_off,
                               unsigned char iv[MBEDTLS_ARIA_BLOCKSIZE],
                               const unsigned char *input,
                               unsigned char *output )
{
//	crypto_aria_context *aria_ctx = (crypto_aria_context *) cryptochip_get_context(ARIA_CTX);
//
//	aria_ctx->moo = ARIA_MODE_BIT_OFB;
//	aria_ctx->isDecrypt = ctx->mode;
//	aria_ctx->key = ctx->key;
//	aria_ctx->keylen = ctx->keylen;
//	aria_ctx->iv = (u8*)iv;
//	aria_ctx->ivlen = MBEDTLS_ARIA_BLOCKSIZE;
//	aria_ctx->addlen = 0;
//	aria_ctx->input = (u8*)input;
//	aria_ctx->output = output;
//	aria_ctx->msglen = length;
//
//	return cryptochip_aria_crypt(aria_ctx);
	return 0;
}

#endif /* MBEDTLS_CIPHER_MODE_OFB */


#if defined(MBEDTLS_CIPHER_MODE_CFB)
/**
 * \brief This function performs an ARIA-CFB128 encryption or decryption
 *        operation.
 *
 *        It performs the operation defined in the \p mode
 *        parameter (encrypt or decrypt), on the input data buffer
 *        defined in the \p input parameter.
 *
 *        For CFB, you must set up the context with mbedtls_aria_setkey_enc(),
 *        regardless of whether you are performing an encryption or decryption
 *        operation, that is, regardless of the \p mode parameter. This is
 *        because CFB mode uses the same key schedule for encryption and
 *        decryption.
 *
 * \note  Upon exit, the content of the IV is updated so that you can
 *        call the same function again on the next
 *        block(s) of data and get the same result as if it was
 *        encrypted in one call. This allows a "streaming" usage.
 *        If you need to retain the contents of the
 *        IV, you must either save it manually or use the cipher
 *        module instead.
 *
 *
 * \param ctx      The ARIA context to use for encryption or decryption.
 * \param mode     The ARIA operation: #MBEDTLS_ARIA_ENCRYPT or
 *                 #MBEDTLS_ARIA_DECRYPT.
 * \param length   The length of the input data.
 * \param iv_off   The offset in IV (updated after use).
 * \param iv       The initialization vector (updated after use).
 * \param input    The buffer holding the input data.
 * \param output   The buffer holding the output data.
 *
 * \return         \c 0 on success.
 */
int mbedtls_aria_crypt_cfb128( mbedtls_aria_context *ctx,
                               int mode,
                               size_t length,
                               size_t *iv_off,
                               unsigned char iv[MBEDTLS_ARIA_BLOCKSIZE],
                               const unsigned char *input,
                               unsigned char *output )
{
//	crypto_aria_context *aria_ctx = (crypto_aria_context *) cryptochip_get_context(ARIA_CTX);
//
//	aria_ctx->moo = ARIA_MODE_BIT_CFB;
//	aria_ctx->isDecrypt = mode ^ 1;
//	aria_ctx->key = ctx->key;
//	aria_ctx->keylen = ctx->keylen;
//	aria_ctx->iv = (u8*)iv;
//	aria_ctx->ivlen = MBEDTLS_ARIA_BLOCKSIZE;
//	aria_ctx->addlen = 0;
//	aria_ctx->input = (u8*)input;
//	aria_ctx->output = output;
//	aria_ctx->msglen = length;
//
//	return cryptochip_aria_crypt(aria_ctx);
	return 0;
}
#endif /* MBEDTLS_CIPHER_MODE_CFB */

#endif

#endif



