/*
 * gcm_alt.h
 *
 *  Created on: Aug 16, 2018
 *      Author: asepawal
 */

#ifndef CRYPTOCHIP_GCM_ALT_H_
#define CRYPTOCHIP_GCM_ALT_H_

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_GCM_ALT)

//ARIA Only
#include "mbedtls/aria.h"

/**
 * \brief          The GCM context structure.
 */
typedef struct mbedtls_gcm_context
{
    mbedtls_cipher_context_t cipher_ctx;  /*!< The cipher context used. */
    mbedtls_aria_context aria;
}
mbedtls_gcm_context;

#endif

#endif /* CRYPTOCHIP_GCM_ALT_H_ */
