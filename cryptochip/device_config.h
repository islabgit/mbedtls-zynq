/*
 * device_config.h
 *
 *  Created on: Aug 30, 2019
 *      Author: asepawal
 */

#ifndef DEVICE_CONFIG_H_
#define DEVICE_CONFIG_H_

#define MBEDTLS_ARIA_ALT
#define MBEDTLS_GCM_ALT
#define MBEDTLS_ECP_MUL_ALT

#endif /* DEVICE_CONFIG_H_ */
