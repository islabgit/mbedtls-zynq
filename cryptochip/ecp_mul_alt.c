
/*  
 * crypto_ecc_alt.c
 *
 *  Created on: 2019. 8. 30.
 *      Author: asepawal
 */

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_ECP_C)

#include "mbedtls/ecp.h"
#include "mbedtls/threading.h"
#include "mbedtls/platform_util.h"
#include "mbedtls/bignum.h"

#include <string.h>

/* Parameter validation macros based on platform_util.h */
#define ECP_VALIDATE_RET( cond )    \
    MBEDTLS_INTERNAL_VALIDATE_RET( cond, MBEDTLS_ERR_ECP_BAD_INPUT_DATA )
#define ECP_VALIDATE( cond )        \
    MBEDTLS_INTERNAL_VALIDATE( cond )

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdlib.h>
#include <stdio.h>
#define mbedtls_printf     printf
#define mbedtls_calloc    calloc
#define mbedtls_free       free
#endif

#if defined(MBEDTLS_ECP_MUL_ALT)

void core_ecc_init( mbedtls_ecp_group *ctx ){
	int fd = open ("/dev/mem", O_RDWR);
	unsigned page_addr, page_offset;
	unsigned page_size = sysconf(_SC_PAGESIZE);
	
	page_addr = (ECC_CORE_BASE_ADDR & (~(page_size-1)));
	page_offset = ECC_CORE_BASE_ADDR - page_addr;
	
	if (fd < 1) {
		printf("ERROR: core_ecc_init()!!\n");
		sleep(3);
		return;
	}
	ctx->hw_ecc = mmap(NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr);
}

/*
 * Restartable multiplication R = m * P
 */
int mbedtls_ecp_mul_restartable( mbedtls_ecp_group *grp, mbedtls_ecp_point *R,
             const mbedtls_mpi *m, const mbedtls_ecp_point *P,
             int (*f_rng)(void *, unsigned char *, size_t), void *p_rng,
             mbedtls_ecp_restart_ctx *rs_ctx )
{
    int ret = MBEDTLS_ERR_ECP_BAD_INPUT_DATA;
    core_ecc *hw_ecc;
    unsigned char Rx_[32], Ry_[32];

    ECP_VALIDATE_RET( grp != NULL );
    ECP_VALIDATE_RET( R   != NULL );
    ECP_VALIDATE_RET( m   != NULL );
    ECP_VALIDATE_RET( P   != NULL );

    if(grp->hw_ecc == NULL)
        core_ecc_init(grp);

    hw_ecc = grp->hw_ecc;

    /* check_privkey is free */
    MBEDTLS_ECP_BUDGET( MBEDTLS_ECP_OPS_CHK );

    /* Common sanity checks */
    MBEDTLS_MPI_CHK( mbedtls_ecp_check_privkey( grp, m ) );
    MBEDTLS_MPI_CHK( mbedtls_ecp_check_pubkey( grp, P ) );

    MBEDTLS_MPI_CHK(mbedtls_mpi_write_binary(m, grp->k, 32));
    MBEDTLS_MPI_CHK(mbedtls_mpi_write_binary(&P->Y, grp->Py, 32));
    MBEDTLS_MPI_CHK(mbedtls_mpi_write_binary(&P->X, grp->Px, 32));
    
    hw_ecc->operation = ECC_CLEAR;
    memcpy(hw_ecc->k, grp->k, 32);
    memset(grp->k, 0, 32);
    memcpy(hw_ecc->Py, grp->Py, 32);
    memcpy(hw_ecc->Px, grp->Px, 32);
    hw_ecc->operation = ECC_ENABLE;
    usleep(1);

    while((ret = hw_ecc->status) == 0){
        msync((void *)hw_ecc->status, sizeof(unsigned int), MS_SYNC);
    };

    MBEDTLS_MPI_CHK(mbedtls_mpi_read_binary(&R->X,hw_ecc->Rx,32));
    MBEDTLS_MPI_CHK(mbedtls_mpi_read_binary(&R->Y,hw_ecc->Ry,32));
    MBEDTLS_MPI_CHK(mbedtls_mpi_lset(&R->Z, 1));

cleanup:
    return( ret );
}

#endif
#endif