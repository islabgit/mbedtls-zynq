/*
 * cbc_alt.h
 *
 *  Created on: 2018. 8. 23.
 *      Author: root
 */

#ifndef CRYPTOCHIP_ECP_MUL_ALT_H_
#define CRYPTOCHIP_ECP_MUL_ALT_H_

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif


#if defined(MBEDTLS_ECP_MUL_ALT)

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#define ECC_CORE_BASE_ADDR 		0x43c10000

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

#define ECC_ENABLE   1 << 0
#define ECC_CLEAR    1 << 1

typedef struct core_ecc {
	u8 name[2*4];
	u8 version[1*4];
	u32 status;
    u32 operation;
    u8 nope[11*4];
    u8 Rx[16*4];
    u8 Ry[16*4];
    u8 k[16*4];
    u8 Py[16*4];
    u8 Px[16*4];
} core_ecc;

#endif
#endif /* CRYPTOCHIP_ECP_MUL_ALT_H_ */
