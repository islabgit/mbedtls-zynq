/*
 * crypto_gcm_alt.c
 *
 *  Created on: Aug 16, 2018
 *      Author: asepawal
 */

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_GCM_C)

#include "mbedtls/gcm.h"
#include "mbedtls/platform_util.h"

#include <string.h>

#if defined(MBEDTLS_ARIA_C)
#include "mbedtls/aria.h"
#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#define mbedtls_printf printf
#endif /* MBEDTLS_PLATFORM_C */
#endif /* MBEDTLS_SELF_TEST && MBEDTLS_AES_C */

#if defined(MBEDTLS_GCM_ALT)

/*
 * Initialize a context
 */
void mbedtls_gcm_init(mbedtls_gcm_context *ctx) {
	memset(ctx, 0, sizeof(mbedtls_gcm_context));

}

int mbedtls_gcm_setkey(mbedtls_gcm_context *ctx, mbedtls_cipher_id_t cipher,
						const unsigned char *key,
						unsigned int keybits) {

	if(cipher != MBEDTLS_CIPHER_ID_ARIA){
		printf("ERROR: GCM is supported only by ARIA Block Cipher!!\n");
		return -1;
	}

	mbedtls_aria_init(&ctx->aria);
	mbedtls_aria_setkey_enc(&ctx->aria, key, keybits);

	return 0;
}

int mbedtls_gcm_crypt_and_tag(mbedtls_gcm_context *ctx, int mode, size_t length,
						const unsigned char *iv, size_t iv_len,
						const unsigned char *add, size_t add_len,
						const unsigned char *input, unsigned char *output,
						size_t tag_len, unsigned char *tag)
{
	u32 i, block;
	ctx->aria.mode |= 1 << ARIA_MODE_BIT_GCM;

	ctx->aria.mode |= iv_len << ARIA_MODE_BIT_IV_SIZE;
	ctx->aria.mode |= tag_len << ARIA_MODE_BIT_MAC_SIZE;

	if (mode) {
		ctx->aria.mode &= ~(1 << ARIA_MODE_BIT_FLAG_DEC);
	} else {
		ctx->aria.mode |= 1 << ARIA_MODE_BIT_FLAG_DEC;
	}

	//operation
	ctx->aria.hw_aria->operation = ARIA_CLEAR;
	ctx->aria.hw_aria->mode = ctx->aria.mode;
	ctx->aria.hw_aria->add_len = add_len;
	ctx->aria.hw_aria->msg_len = length;
	ctx->aria.hw_aria->operation = ARIA_INIT;
	memcpy(ctx->aria.hw_aria->key, ctx->aria.key, 32);
	ctx->aria.hw_aria->operation = ARIA_UPDATE_KEY;

	memcpy(ctx->aria.hw_aria->blk_in, iv, iv_len);
	ctx->aria.hw_aria->operation = ARIA_UPDATE_IV;

	for(i = 0; i < add_len; i += MBEDTLS_ARIA_BLOCKSIZE){
		if((block = add_len - i) > MBEDTLS_ARIA_BLOCKSIZE)
			block = MBEDTLS_ARIA_BLOCKSIZE;

		memcpy(ctx->aria.hw_aria->blk_in, add + i, block);
		ctx->aria.hw_aria->operation = ARIA_UPDATE_ADD;usleep(1);
	}

	for(i = 0; i < length; i += MBEDTLS_ARIA_BLOCKSIZE){
		if((block = length - i) > MBEDTLS_ARIA_BLOCKSIZE)
			block = MBEDTLS_ARIA_BLOCKSIZE;

		memcpy(ctx->aria.hw_aria->blk_in, input + i, block);
		ctx->aria.hw_aria->operation = ARIA_UPDATE_BLK;usleep(1);
		memcpy(output + i, ctx->aria.hw_aria->blk_out, block);
	}

	memcpy(tag, ctx->aria.hw_aria->mac_out, tag_len);
	return 0;
}

int mbedtls_gcm_auth_decrypt(mbedtls_gcm_context *ctx, size_t length,
						const unsigned char *iv, size_t iv_len,
						const unsigned char *add, size_t add_len,
						const unsigned char *tag, size_t tag_len,
						const unsigned char *input, unsigned char *output)
{
	int ret;
	unsigned char check_tag[16];
	size_t i;
	int diff;

	if ((ret = mbedtls_gcm_crypt_and_tag(ctx, MBEDTLS_GCM_DECRYPT, length, iv,
			iv_len, add, add_len, input, output, tag_len, check_tag)) != 0) {
		return (ret);
	}

	/* Check tag in "constant-time" */
	for (diff = 0, i = 0; i < tag_len; i++)
		diff |= tag[i] ^ check_tag[i];

	if (diff != 0) {
		// mbedtls_platform_zeroize(output, length);
		memset(output, 0, length);
		return ( MBEDTLS_ERR_GCM_AUTH_FAILED);
	}

	return (0);
}

int mbedtls_gcm_starts(mbedtls_gcm_context *ctx, int mode,
						const unsigned char *iv, size_t iv_len,
						const unsigned char *add, size_t add_len) {
//	crypto_aria_context *aria_ctx = (crypto_aria_context *) cryptochip_get_context(ARIA_CTX);
//
//	aria_ctx->moo = ARIA_MODE_BIT_GCM;
//	aria_ctx->isDecrypt = mode ^ 1;
//	aria_ctx->key = ctx->key;
//	aria_ctx->keylen = ctx->keylen;
//	aria_ctx->iv = (u8*)iv;
//	aria_ctx->ivlen = iv_len;
//	aria_ctx->add = (u8*)add;
//	aria_ctx->addlen = add_len;
	return -1;
}

int mbedtls_gcm_update(mbedtls_gcm_context *ctx, size_t length,
						const unsigned char *input, unsigned char *output) {
//	crypto_aria_context *aria_ctx = (crypto_aria_context *) cryptochip_get_context(ARIA_CTX);
//	aria_ctx->input = (u8*)input;
//	aria_ctx->output = output;
//	aria_ctx->msglen = length;
//	(void) ctx;
	return -1;
}

void mbedtls_gcm_free(mbedtls_gcm_context *ctx) {
	// mbedtls_cipher_free(&ctx->cipher_ctx);
	// mbedtls_platform_zeroize(ctx, sizeof(mbedtls_gcm_context));
	if( &ctx->cipher_ctx != NULL )
		memset(&ctx->cipher_ctx, 0, sizeof(mbedtls_cipher_context_t));
	memset(ctx, 0, sizeof(mbedtls_gcm_context));
}

int mbedtls_gcm_finish(mbedtls_gcm_context *ctx, unsigned char *tag,
						size_t tag_len) {
//	crypto_aria_context *aria_ctx = (crypto_aria_context *) cryptochip_get_context(ARIA_CTX);
//	aria_ctx->tag = tag;
//	aria_ctx->taglen = tag_len;
//	cryptochip_aria_crypt(aria_ctx);
	return -1;
}

#endif /* MBEDTLS_GCM_ALT */
#endif /* MBEDTLS_GCM_C */

