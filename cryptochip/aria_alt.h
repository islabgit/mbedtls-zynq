/*
 * cbc_alt.h
 *
 *  Created on: 2018. 8. 23.
 *      Author: root
 */

#ifndef CRYPTOCHIP_ARIA_ALT_H_
#define CRYPTOCHIP_ARIA_ALT_H_

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif
#include "mbedtls/platform.h"

#if defined(MBEDTLS_ARIA_ALT)

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>


#define STATUS_RX_READY    	0
#define STATUS_TX_READY    	1

#define ARIA_INIT 				1 << 0
#define ARIA_CLEAR 				1 << 1
#define ARIA_UPDATE_KEY 		1 << 2
#define ARIA_UPDATE_ADD 		1 << 4
#define ARIA_UPDATE_BLK 		1 << 8
#define ARIA_UPDATE_IV 			1 << 16

#define ARIA_MODE_ENC 			0
#define ARIA_MODE_DEC 			1

#define ARIA_KEY_128 			1
#define ARIA_KEY_192 			2
#define ARIA_KEY_256 			3

#define ARIA_MODE_BIT_KEY_SIZE	0
#define ARIA_MODE_BIT_FLAG_DEC	2
#define ARIA_MODE_BIT_MAC_SIZE	3
#define ARIA_MODE_BIT_CMAC		8
#define ARIA_MODE_BIT_GCM		9
#define ARIA_MODE_BIT_CCM 		10
#define ARIA_MODE_BIT_CTR		11
#define ARIA_MODE_BIT_OFB		12
#define ARIA_MODE_BIT_CFB		13
#define ARIA_MODE_BIT_CBC		14
#define ARIA_MODE_BIT_ECB		15
#define ARIA_MODE_BIT_IV_SIZE	16


#define CORE_NAME0      0x41524941 //ARIA
#define CORE_NAME1      0x4d4f4f00 //MOO
#define CORE_VERSION    0x76312e30 //v1.1

#define CORE_BASE_ADDR 		0x43c00000

typedef struct core_aria {
	u8 name[8];
	u8 version[4];
	u32 status;
	u8 blk_out[16];
	u8 mac_out[16];
	u32 add_len;
	u32 msg_len;
	u8 key[32];
	u8 blk_in[16];
	u32 mode;
	u32 operation;
} core_aria;


/**
 * \brief          The GCM context structure.
 */
typedef struct mbedtls_aria_context
{
	struct core_aria *hw_aria;
	u8 key[32];
	u32 keylen;
	u32 mode;
} mbedtls_aria_context;

#endif
#endif /* CRYPTOCHIP_ARIA_ALT_H_ */
