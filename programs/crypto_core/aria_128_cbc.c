/*
 * aria_128_cbc.c
 *
 *  Created on: Aug 29, 2019
 *      Author: asepawal
 */


#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "mbedtls/aria.h"

void s2b(char *fromString, u8* toByte, size_t len) {
	size_t count = 0;
	for (; count < len; count++) {
		sscanf(fromString, "%2hhx", &toByte[count]);
		fromString += 2;
	}
}

void print_buf(unsigned char buf[], size_t len) {
	size_t i;
	for (i=0; i < len; i++) {
		printf("%02X", buf[i]);
	}
	printf("\n");
}

int main(){
	mbedtls_aria_context aria;

	char* KEY = "E6922558E9EF2071C13714726599422B";
	char* IV = "B096F842031358E032AC5AAA9E4C15F3";
	char *PLAIN = "0AD42CF2AE7109FDB1F403C671A4C85A"
			"66014AD1145F0D85E42542656997D5B5"
			"AAED66CDF1AE8CC8B3580E46EDADF749";

	char *CIPHER = "9EFA3A2F24F6E1AB9D05887C180958B6"
			"ABD4ABDE123ECCA19A75FDC53E95BF61"
			"0F794D49D9D06ACD03735CA3B7F63921";

	u8 KEY_IN[32], IV_IN[16], PLAIN_OUT[48], PLAIN_IN[48], CIPHER_OUT[48], CIPHER_IN[48];
	u8 KEY_LEN = 16, IV_LEN = 16, MSG_LEN = 48;
	int status;

	memset(KEY_IN, 0, 32);
	s2b(KEY, KEY_IN, KEY_LEN);
	s2b(IV, IV_IN, IV_LEN);
	s2b(PLAIN, PLAIN_IN, MSG_LEN);
	s2b(CIPHER, CIPHER_IN, MSG_LEN);


	mbedtls_aria_init(&aria);
	mbedtls_aria_setkey_enc( &aria, KEY_IN, KEY_LEN*8 );
	mbedtls_aria_crypt_cbc(&aria, MBEDTLS_ARIA_DECRYPT, MSG_LEN, IV_IN, PLAIN_IN, CIPHER_OUT);

	printf("CIPHER ACTUAL   : ");
	print_buf(CIPHER_OUT, MSG_LEN);
	printf("CIPHER Expected : ");
	print_buf(CIPHER_IN, MSG_LEN);

	status = memcmp(CIPHER_OUT, CIPHER_IN, MSG_LEN);
	if (status) {
		printf("TEST FAILED!! \n");
		return -1;
	} else {
		printf("TEST SUCCESS!!\n");
	}

	mbedtls_aria_crypt_cbc(&aria, MBEDTLS_ARIA_ENCRYPT, MSG_LEN, IV_IN, CIPHER_IN, PLAIN_OUT);

	printf("CIPHER ACTUAL   : ");
	print_buf(PLAIN_OUT, MSG_LEN);
	printf("CIPHER Expected : ");
	print_buf(PLAIN_IN, MSG_LEN);

	status = memcmp(PLAIN_OUT, PLAIN_IN, MSG_LEN);
	if (status) {
		printf("TEST FAILED!! \n");
		return -1;
	} else {
		printf("TEST SUCCESS!!\n");
	}

	return 0;
}




