/*
 * aria_128_gcm.c
 *
 *  Created on: Aug 29, 2019
 *      Author: asepawal
 */


#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#include "mbedtls/platform_util.h"
#else
#include <stdio.h>
#include <stdlib.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "mbedtls/gcm.h"

void s2b(char *fromString, u8* toByte, size_t len) {
	size_t count = 0;
	for (; count < len; count++) {
		sscanf(fromString, "%2hhx", &toByte[count]);
		fromString += 2;
	}
}

void print_buf(unsigned char buf[], size_t len) {
	size_t i;
	for (i=0; i < len; i++) {
		printf("%02X", buf[i]);
	}
	printf("\n");
}

int main(){
	mbedtls_gcm_context aria_gcm;

	char* KEY = "BE96C0A194E5A8AC63E695555D5958F4";
	char* NONCE = "0CEFF05A88AD5E5AED92B588";
	char* ADD = "21E531FA45604E3516A1A421CA5E27B0"
			"1AB783AB3B5DB2F710F9CD169ECC22F0"
			"C09288D41CADEFBA557F3BCB83185323"
			"E01174D5C3B7CC2FD36D4440744B9D98"
			"860BBE4F4F264C49B53A7CB7B1AF25E1"
			"009A1A241CE1B6366AA1B8B2B5CC51D0"
			"DB177C73C6138E6A9E990EEF3D6CC674"
			"E41A1AA02C2399943F5DD27247976920";

	char *PLAIN = "0C797140614109EA462AC87D6582D842"
			"7382A0DB1C7350833914B99749406356"
			"1FCCD9E5D8AF4895106548695695FAD3"
			"DCF153BF735DB7D2B754CAF48788830C"
			"B9C9820A0B28A32B5C5CD3781C652291"
			"016F1AA8FBF84FD06B35387890B23D32"
			"413A11BAE2F6423391D80FB3A03B7902"
			"48C39D9F9B8B4004BB7FAC2B4A06BB51";

	char *CIPHER = "F1AC68229DF0DFD16671ADE5D40C7CB7"
			"23BC1B2F548AB0E97F887D9E487F4519"
			"A35C6CD6FFFBCD8FCEE9E19B4BFA0484"
			"8A58B4582EBE9E0E965AECBF7A16C577"
			"C5851B113C2F0B09CAC1225969CA6B85"
			"00DC9EDE950B30A9424DB512783E9803"
			"2764395BDEA2AE338E6F38AA68606BA8"
			"7738F659BBABA48DEB198CAD924CD39C";

	char *MAC = "1F793DD9BBF206C81E0B46B25D500328";


	u8 KEY_IN[32], IV_IN[16], ADD_IN[128], PLAIN_OUT[128], PLAIN_IN[128], CIPHER_OUT[128], CIPHER_IN[128], TAG_OUT[16], TAG_IN[16];
	u8 KEY_LEN = 16, IV_LEN = 12, ADD_LEN = 128, MSG_LEN = 128, TAG_LEN = 16;
	int status;

	memset(KEY_IN, 0, 32);
	s2b(KEY, KEY_IN, 16);
	s2b(NONCE, IV_IN, 12);
	s2b(ADD, ADD_IN, 128);
	s2b(PLAIN, PLAIN_IN, 128);
	s2b(CIPHER, CIPHER_IN, 128);
	s2b(MAC, TAG_IN, 16);


	mbedtls_gcm_init(&aria_gcm);
	mbedtls_gcm_setkey(&aria_gcm, MBEDTLS_CIPHER_ID_ARIA, KEY_IN, KEY_LEN*8);

	mbedtls_gcm_crypt_and_tag(&aria_gcm,
								MBEDTLS_GCM_ENCRYPT,
								MSG_LEN,
								IV_IN, IV_LEN,
								ADD_IN, ADD_LEN,
								PLAIN_IN, CIPHER_OUT,
								TAG_LEN, TAG_OUT);

	printf("CIPHER ACTUAL   : ");
	print_buf(CIPHER_OUT, 128);
	printf("CIPHER Expected : ");
	print_buf(CIPHER_IN, 128);

	status = memcmp(CIPHER_OUT, CIPHER_IN, 128);
	if (status) {
		printf("TEST FAILED!! \n");
		return -1;
	} else {
		printf("TEST SUCCESS!!\n");
	}

	printf("TAG ACTUAL   : ");
	print_buf(TAG_OUT, 16);
	printf("TAG Expected : ");
	print_buf(TAG_IN, 16);

	status = memcmp(TAG_OUT, TAG_IN, 16);
	if (status) {
		printf("TEST FAILED!! \n");
		return -1;
	} else {
		printf("TEST SUCCESS!!\n");
	}

	mbedtls_gcm_crypt_and_tag(&aria_gcm,
								MBEDTLS_GCM_DECRYPT,
								MSG_LEN,
								IV_IN, IV_LEN,
								ADD_IN, ADD_LEN,
								CIPHER_IN, PLAIN_OUT,
								TAG_LEN, TAG_OUT);

	printf("CIPHER ACTUAL   : ");
	print_buf(PLAIN_OUT, 128);
	printf("CIPHER Expected : ");
	print_buf(PLAIN_IN, 128);

	status = memcmp(PLAIN_OUT, PLAIN_IN, 128);
	if (status) {
		printf("TEST FAILED!! \n");
		return -1;
	} else {
		printf("TEST SUCCESS!!\n");
	}

	printf("TAG ACTUAL   : ");
	print_buf(TAG_OUT, 16);
	printf("TAG Expected : ");
	print_buf(TAG_IN, 16);

	status = memcmp(TAG_OUT, TAG_IN, 16);
	if (status) {
		printf("TEST FAILED!! \n");
		return -1;
	} else {
		printf("TEST SUCCESS!!\n");
	}

	return 0;
}


