

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "cryptochip/ecp_mul_alt.h"

#if !defined(MBEDTLS_ECP_MUL_ALT)
int main(){
	printf("MBEDTLS_ECP_MUL_ALT is disabled!!");
	return 0;
}
#else

void s2b(char *fromString, u8* toByte, size_t len) {
	size_t count = 0;
	for (; count < len; count++) {
		sscanf(fromString, "%2hhx", &toByte[count]);
		fromString += 2;
	}
}

void print_buf(unsigned char buf[], size_t len) {
	size_t i;
	for (i=0; i < len; i++) {
		printf("%02X", buf[i]);
	}
	printf("\n");
}

int main(){
	core_ecc *hw_ecc;

	char* K = "4dfa12defc60319021b681b3ff84a10a511958c850939ed45635934ba4979147";
	char* Px = "2c91c61f33adfe9311c942fdbff6ba47020feff416b7bb63cec13faf9b099954";
	char *Py = "6cab31b06419e5221fca014fb84ec870622a1b12bab5ae43682aa7ea73ea08d0";
    //#3ca1fc7ad858fb1a6aba232542f3e2a749ffc7203a2374a3f3d3267f1fc97b78

	unsigned char K_[32], Px_[32], Py_[32], Rx_[32], Ry_[32];

    int fd = open ("/dev/mem", O_RDWR);
	unsigned page_addr;
	unsigned page_size = sysconf(_SC_PAGESIZE);
    int ret;
	
	page_addr = (ECC_CORE_BASE_ADDR & (~(page_size-1)));
	// page_offset = ECC_CORE_BASE_ADDR - page_addr;
	
	if (fd < 1) {
		printf("ERROR: core_ecc_init()!!\n");
		sleep(3);
		return -1;
	}
	hw_ecc = mmap(NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr);

	s2b(K, K_, 32);
	s2b(Px, Px_, 32);
	s2b(Py, Py_, 32);

    hw_ecc->operation = ECC_CLEAR;
    memcpy(hw_ecc->k, K_, 32);
    memcpy(hw_ecc->Px, Px_, 32);
    memcpy(hw_ecc->Py, Py_, 32);
    hw_ecc->operation = ECC_ENABLE;
    
    printf("Wait..\n");

    while((ret = hw_ecc->status) == 0){
        msync((void *)hw_ecc->status, sizeof(unsigned int), MS_SYNC);
    };


    printf("k:\n");
    print_buf(K_, 32);
    printf("Px:\n");
    print_buf(Px_, 32);
    printf("Py:\n");
    print_buf(Py_, 32);

    printf("Rx:\n");
    memcpy(Rx_, hw_ecc->Rx, 32);
    print_buf(Rx_, 32);
    
    printf("Ry:\n");
    memcpy(Ry_, hw_ecc->Ry, 32);
    print_buf(Ry_, 32);

	return 0; 
}
#endif




