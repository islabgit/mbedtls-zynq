/*
 * aria_128_ecb.c
 *
 *  Created on: Aug 28, 2019
 *      Author: asepawal
 */


#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#include <stdlib.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "mbedtls/aria.h"

void s2b(char *fromString, u8* toByte, size_t len) {
	size_t count = 0;
	for (; count < len; count++) {
		sscanf(fromString, "%2hhx", &toByte[count]);
		fromString += 2;
	}
}

void print_buf(unsigned char buf[], size_t len) {
	size_t i;
	for (i=0; i < len; i++) {
		printf("%02X", buf[i]);
	}
	printf("\n");
}

int main(){
	mbedtls_aria_context aria;

	char* KEY = "7AEC775F7D4F493F1EF020CD7BFEBFD0";
	char *PLAIN = "158B43008F3F065FB349C4CDB69BFCD7";
	char *CIPHER = "DB201B87BDC10D615DFC33BE4C0E75F5";

	u8 KEY_IN[32], PLAIN_IN[32], CIPHER_IN[32];
	u8 CIPHER_OUT[32];
	u8 KEY_LEN = 16, MSG_LEN = 16;
	int status;

	memset(KEY_IN, 0, 32);
	s2b(KEY, KEY_IN, KEY_LEN);
	s2b(PLAIN, PLAIN_IN, MSG_LEN);
	s2b(CIPHER, CIPHER_IN, MSG_LEN);

	mbedtls_aria_init(&aria);
	mbedtls_aria_setkey_enc( &aria, KEY_IN, KEY_LEN*8 );
	mbedtls_aria_crypt_ecb( &aria, PLAIN_IN, CIPHER_OUT );

	printf("CIPHER ACTUAL   : ");
	print_buf(CIPHER_OUT, MSG_LEN);
	printf("CIPHER Expected : ");
	print_buf(CIPHER_IN, MSG_LEN);

	status = memcmp(CIPHER_OUT, CIPHER_IN, MSG_LEN);
	if (status) {
		printf("TEST FAILED!! \n");
		return -1;
	} else {
		printf("TEST SUCCESS!!\n");
	}


	return 0;
}
